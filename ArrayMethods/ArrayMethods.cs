﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ArrayMethodsNamespace
{
    public class ArrayMethods
    {
        private static readonly int arrSize = 10;
        private readonly IRandomGenerator randomGenerator;

        public ArrayMethods(IRandomGenerator generator)
        {
            randomGenerator = generator;
        }

        public int[] CreateRandomArray()
        {
            var arr = new int[10];
            
            for (int i = 0; i < arrSize; i++)
            {
                arr[i] = randomGenerator.Generate();
            }

            return arr;
        }

        public int[] MultiplyByRandomNumber(int[] arr)
        {
            if (arr is null)
            {
                throw new ArgumentNullException(nameof(arr));
            }

            if (arr.Length != 10)
            {
                throw new ArgumentException("Array must be of size 10", nameof(arr));
            }

            var randomNumber = randomGenerator.Generate();


            for (int i = 0; i < arrSize; i++)
            {
                arr[i] *= randomNumber;
            }

            return arr;
        }

        public static int[] SortArrayByAscending(int[] arr)
        {
            if (arr is null)
            {
                throw new ArgumentNullException(nameof(arr));
            }

            if (arr.Length != 10)
            {
                throw new ArgumentException("Array must be of size 10", nameof(arr));
            }

            Array.Sort(arr);

            return arr;
        }

        public static double CalculateAverageValue(int[] arr)
        {
            if (arr is null)
            {
                throw new ArgumentNullException(nameof(arr));
            }

            if (arr.Length != 10)
            {
                throw new ArgumentException("Array must be of size 10", nameof(arr));
            }

            double average = 0;

            for (int i = 0; i < arrSize; i++)
            {
                average += arr[i];
            }

            return average / arrSize;
        }
    }
}
