using Microsoft.VisualStudio.TestPlatform.TestHost;
using Moq;

namespace ArrayMethodsTests
{
    public class ArrayMethodsClassTests
    {
        private static readonly int arrSize = 10;
        private static readonly int randomNumber = 5;
        private static int[] arr = new int[] {2, 4, 5, 1, 3, 7, 8, 9, 10, 6};
        private static ArrayMethods? classToTest;

        public ArrayMethodsClassTests() 
        {
            arr = Enumerable.Range(1, arrSize).ToArray();
            var mockRandomGenerator = new Mock<IRandomGenerator>();
            mockRandomGenerator.Setup(x => x.Generate()).Returns(randomNumber);
            classToTest = new ArrayMethods(mockRandomGenerator.Object);
        }

        [Fact]
        public void ArrayMethods_CreateRandomArray_ReturnsIntArrayOfSizeTen()
        {
            var methodArr = classToTest!.CreateRandomArray();
            for (int i = 0; i < methodArr.Length; i++)
            {
                Assert.Equal(randomNumber, methodArr[i]);
            }
            Assert.Equal(arrSize, methodArr.Length);
        }

        [Fact]
        public void ArrayMethods_MultiplyByRandomNumber_MultipliesByRandomNumber()
        {
            var result = classToTest!.MultiplyByRandomNumber(arr);
            for (int i = 0; i < arr.Length; i++)
            {
                Assert.Equal((i + 1) * randomNumber, result[i]);
            }
        }

        [Theory]
        [InlineData(null)]
        [InlineData(new int[] { })]
        [InlineData(new int[] { 1 })]
        [InlineData(new int[] { 1, 2, 3, 4, 5 })]
        [InlineData(new int[] { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11 })]
        public void ArrayMethods_MultiplyByRandomNumberArrayIsNullOrEmptyOrIncorrectLength_ThrowsException(int[] arr)
        {
            if (arr is null)
            {
                #pragma warning disable CS8604 // Possible null reference argument.
                Assert.Throws<ArgumentNullException>(() => classToTest!.MultiplyByRandomNumber(arr));
                #pragma warning restore CS8604 // Possible null reference argument.
            }
            else
            {
                Assert.Throws<ArgumentException>(() => classToTest!.MultiplyByRandomNumber(arr));
            }
        }

        [Fact]
        public void ArrayMethods_SortArrayByAscending_SortedCorrectly()
        {
            var sortedArray = new int[] { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 };
            var result = ArrayMethods.SortArrayByAscending(arr);

            for (int i = 0; i < arr.Length; i++)
            {
                Assert.Equal(sortedArray[i], result[i]);
            }
        }

        [Theory]
        [InlineData(null)]
        [InlineData(new int[] { })]
        [InlineData(new int[] { 1 })]
        [InlineData(new int[] { 1, 2, 3, 4, 5 })]
        [InlineData(new int[] { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11 })]
        public void ArrayMethods_SortArrayByAscendingArrayIsNullOrEmptyOrIncorrectLength_ThrowsException(int[] arr)
        {
            if (arr is null)
            {
                #pragma warning disable CS8604 // Possible null reference argument.
                Assert.Throws<ArgumentNullException>(() => ArrayMethods.SortArrayByAscending(arr));
                #pragma warning restore CS8604 // Possible null reference argument.
            }
            else
            {
                Assert.Throws<ArgumentException>(() => ArrayMethods.SortArrayByAscending(arr));
            }
        }

        [Fact]
        public void ArrayMethods_CalculateAverageValue_CalculateCorrectAvarage()
        {
            double avarage = arr.Sum() / (double)arr.Length;

            Assert.Equal(avarage, ArrayMethods.CalculateAverageValue(arr));
        }

        [Theory]
        [InlineData(null)]
        [InlineData(new int[] { })]
        [InlineData(new int[] { 1 })]
        [InlineData(new int[] { 1, 2, 3, 4, 5 })]
        [InlineData(new int[] { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11 })]
        public void ArrayMethods_CalculateAverageValueArrayIsNullOrEmptyOrIncorrectLength_ThrowsException(int[] arr)
        {
            if (arr is null)
            {
                #pragma warning disable CS8604 // Possible null reference argument.
                Assert.Throws<ArgumentNullException>(() => ArrayMethods.CalculateAverageValue(arr));
                #pragma warning restore CS8604 // Possible null reference argument.
            }
            else
            {
                Assert.Throws<ArgumentException>(() => ArrayMethods.CalculateAverageValue(arr));
            }
        }
    }
}