﻿using ArrayMethodsNamespace;

var intRandomGenerator = new IntRandomGenerator();
var arrayMethods = new ArrayMethods(intRandomGenerator);

var task1 = Task.Run(() =>
    {
        var arr = arrayMethods.CreateRandomArray();
        Console.Write("Randomly created ");
        PrintArray(arr);
        return arr;
    });
var task2 = task1.ContinueWith((antecedent) =>
    {
        var arr = arrayMethods.MultiplyByRandomNumber(antecedent.Result);
        Console.Write("Multiplied by random number ");
        PrintArray(arr);
        return arr;
    });
var task3 = task2.ContinueWith((antecedent) =>
    {
        var arr = ArrayMethods.SortArrayByAscending(antecedent.Result);
        Console.Write("Sorted by ascending ");
        PrintArray(arr);
        return arr;
    });
var task4 = task3.ContinueWith((antecedent) =>
{
    var average = ArrayMethods.CalculateAverageValue(antecedent.Result);
    Console.WriteLine($"Average is: {average}");
    return average;
});

var avarage = await task4;
Console.ReadLine();

static void PrintArray(int[] arr)
{
    Console.WriteLine("array:");
    foreach(var i in arr)
    {
        Console.WriteLine(i);
    }
    Console.WriteLine("----------");
}